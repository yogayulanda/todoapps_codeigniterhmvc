<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Todoapp extends CI_Controller {
	public function index(){
        $this->load->view('../template/header');
        $this->load->view('v_todo');
        $this->load->view('../template/footer');

    }
    	public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect('login');
	}
}