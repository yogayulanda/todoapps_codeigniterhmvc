<!DOCTYPE html>
<html>
<head>
	<title>CRUD CI | Siswa</title>
</head>
<body>
	<center>
		<h1>Data Siswa</h1>
		<h3>Tambah</h3>
	</center>
	<form action="<?php echo base_url(). 'siswa/tambah_aksi'; ?>" method="post">
		<table style="margin:20px auto;">
			<tr>
				<td>Nama</td>
				<td><input type="text" name="nama"></td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td><input type="text" name="alamat"></td>
			</tr>
			<tr>
				<td>Kelas</td>
				<td><input type="text" name="kelas"></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="Tambah"></td>
			</tr>
		</table>
	</form>
</body>
</html>
