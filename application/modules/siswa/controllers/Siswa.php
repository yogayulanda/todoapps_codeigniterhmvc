<?php

class Siswa extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Siswa_model');

	}

	function index()
	{
		$data['siswa'] = $this->Siswa_model->get_siswa()->result();
		$this->load->view('view_siswa',$data);
	}

	function tambah()
	{
		$this->load->view('view_input');
	}

  function tambah_aksi()
	{
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$kelas = $this->input->post('kelas');

		$data = array(
			'nama' => $nama,
			'alamat' => $alamat,
			'kelas' => $kelas
			);
		$this->Siswa_model->input_data($data,'siswa');
		redirect('siswa');
	}

	function edit($id)
	{
		$where = array('id' => $id);
		$data['siswa'] = $this->Siswa_model->edit_data($where,'siswa')->result();
		$this->load->view('view_edit',$data);
	}

	function update()
	{
		$id = $this->input->post('id');
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$kelas = $this->input->post('kelas');

		$data = array(
			'nama' => $nama,
			'alamat' => $alamat,
			'kelas' => $kelas
		);

		$where = array(
			'id' => $id
		);

		$this->Siswa_model->update_data($where,$data,'siswa');
		redirect('siswa');
	}

	function hapus($id)
	{
		$where = array('id' => $id);
		$this->Siswa_model->hapus_data($where,'siswa');
		redirect('siswa');
	}

}
