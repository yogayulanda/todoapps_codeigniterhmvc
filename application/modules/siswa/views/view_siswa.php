<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Latihan - Codigniter</title>
  </head>
  <body>
    <center>
      <h1>Data Siswa</h1>
    </center>
    <center><?php echo anchor('siswa/tambah','Tambah Data'); ?></center>
    <table style="margin:20px auto;" border="1">
      <thead>
        <tr>
            <td>No.</td>
            <td>Nama</td>
            <td>Alamat</td>
            <td>Kelas</td>
            <td>Aksi</td>
        </tr>
      </thead>
      <tbody>
      <?php
      $no = 1;
      foreach ($siswa as $sis) {?>
        <tr>
          <td><?php echo $no; ?></td>
          <td><?php echo $sis->nama; ?></td>
          <td><?php echo $sis->alamat; ?></td>
          <td><?php echo $sis->kelas; ?></td>
          <td>
            <?php echo anchor('siswa/edit/'.$sis->id,'Edit'); ?>
            <?php echo anchor('siswa/hapus/'.$sis->id,'Hapus'); ?>
          </td>
        </tr>
      <?php } ?>
    </tbody>
    </table>
  </body>
</html>
