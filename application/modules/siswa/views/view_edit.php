<!DOCTYPE html>
<html>
<head>
	<title>siswa CI - Siswa</title>
</head>
<body>
	<center>
		<h1>Edit Data Siswa</h1>
		<h3>Edit Data</h3>
	</center>
	<?php foreach($siswa as $sis){ ?>
	<form action="<?php echo base_url(). 'siswa/update'; ?>" method="post">
		<table style="margin:20px auto;">
			<tr>
				<td>Nama</td>
				<td>
					<input type="hidden" name="id" value="<?php echo $sis->id ?>">
					<input type="text" name="nama" value="<?php echo $sis->nama ?>">
				</td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td><input type="text" name="alamat" value="<?php echo $sis->alamat ?>"></td>
			</tr>
			<tr>
				<td>Kelas</td>
				<td><input type="text" name="kelas" value="<?php echo $sis->kelas ?>"></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="Simpan"></td>
			</tr>
		</table>
	</form>
	<?php } ?>
</body>
</html>
