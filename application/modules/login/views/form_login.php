<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <style>
        
        }
        .intro, .__form{
            height: 100%;
            display: inline-block;
            float: left;
        }
        .intro{
            text-align: center;
            width: calc( 100% - 400px );
            border-right: 1px solid #e3e3e3;
            box-sizing: border-box;
            position: relative;
        }
        .__form{
            width: 400px;
            position: relative;
            float: right;
            padding: 15px;
        }
        .intro h1{
            margin-top: 50px;
        }
        .signin-image{
            width: 100%;
            max-width: 400px;
        }
        .__form .ui.form{
            /*position: absolute;
            width: 400px;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin: auto;
            height: 700px;*/
        }
        .about{
            font-size: 16px;
            padding: 20px;
        }
        .message-wrapper{
            padding: 10px 20px;
            border: 1px solid #e3e3e3;
            margin: 10px 0;
            border-radius: 3px;
        }
        .message-wrapper.error{
            background: #ffacac;
            border: 2px solid #ec7171;
        }
        .message-wrapper.success{
            background: #57de94;
            border: 2px solid #3a9c66;
        }
        .message{
            text-align: center;
        }
        .message-wrapper.error .message{
            color: #ce3131;
            font-size: 16px;
        }
        .message-wrapper.success .message{
            color: #266b45;
            font-size: 16px;
        }
        .signin-options{
            margin: 20px 0;
            text-align: center;
            margin-top: 60px;
        }
        .signin-options .signin-btn{
            width: 200px;
            display: inline-block;
            text-align: center;
            padding: 10px 40px;
            font-size: 20px;
            text-decoration: underline;
        }
        .ui.form .field {
            clear: both;
            margin: 0 0 1em;
        }
    </style>


</head>
<body>
    <div id="login">
        <h3 class="text-center text-white pt-5">Welcome TO DO App</h3>
        <div class="container">
        <?php echo form_open("login/cek_login"); ?>
        <?php echo $error; ?>
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" class="form" action="" method="post">
                            <h3 class="text-center text-info">Login</h3>
                            <div class="form-group">
                                <label for="username" class="text-info">Username:</label><br>
                                <input type="text" name="username" id="username" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password" class="text-info">Password:</label><br>
                                <input type="text" name="password" id="password" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="remember-me" class="text-info"><span>Remember me</span> <span><input id="remember-me" name="remember-me" type="checkbox"></span></label><br>
                                <input type="submit" name="submit" class="btn btn-info btn-md" value="submit">
                           </div>

        <?php echo form_close(); ?>   
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body></html>