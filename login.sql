-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 20 Sep 2019 pada 18.17
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `login`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_buku`
--

CREATE TABLE `tbl_buku` (
  `id_buku` int(11) NOT NULL,
  `nama_buku` varchar(100) DEFAULT NULL,
  `tanggaL_terbit` date DEFAULT NULL,
  `pengarang` varchar(100) DEFAULT NULL,
  `flag` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_buku`
--

INSERT INTO `tbl_buku` (`id_buku`, `nama_buku`, `tanggaL_terbit`, `pengarang`, `flag`) VALUES
(1, 'TKJ', '2019-06-01', 'Ahmad', 0),
(2, 'RPL', '2019-06-02', 'Ali', 0),
(4, 'IPA', '2019-06-03', 'Iim', 0),
(6, 'as', '2019-05-29', 'zxc', 0),
(7, 'Mancing', '2019-09-20', 'Pergi Memancing', 0),
(8, 'Mancing', '2019-09-20', 'Pergi Memancing', 1),
(9, 'mybook', '2019-07-03', 'septi dan iim', 0),
(10, 'mybook', '2019-07-03', 'septi dan iim', 0),
(11, 'mybook2', '2019-07-17', 'iim', 0),
(12, '\'mybook2\'', '0000-00-00', '\'sdvcd\'', NULL),
(13, '\'mybook2\'', '0000-00-00', '\'septi dan iim\'', NULL),
(14, 's', '2019-07-11', 's', 0),
(15, 'kuliah', '2019-09-21', 'Ruang 543', 0),
(16, 'kuliah', '2019-09-20', 'fgfhgf', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_todo`
--

CREATE TABLE `t_todo` (
  `id_kegiatan` int(11) NOT NULL,
  `nama_kegiatan` varchar(100) NOT NULL,
  `tanggal_kegiatan` date NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `flag` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `username` varchar(250) DEFAULT NULL,
  `password` text DEFAULT NULL,
  `level` enum('admin','member') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `level`) VALUES
(2, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin'),
(3, 'member', '827ccb0eea8a706c4c34a16891f84e7b', 'member'),
(4, 'yoga', '25d55ad283aa400af464c76d713c07ad', 'member');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_buku`
--
ALTER TABLE `tbl_buku`
  ADD PRIMARY KEY (`id_buku`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_buku`
--
ALTER TABLE `tbl_buku`
  MODIFY `id_buku` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
